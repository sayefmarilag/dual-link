export const routes = [
  '/',
  '/about',
  '/contacts',
  '/tutorials',
  '/blog',
  '/clients',
  '/partners',
  '/pos/features',
  '/pos/bar-and-restaurant',
  '/pos/retail'
]
